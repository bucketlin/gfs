#ifndef GFS_MASTER_H
#define GFS_MASTER_H

#include "common.h"
#include "global.h"
#include <string>
#include <service.hpp>
#include <unordered_map>
#include <map>
#include <vector>
#include <set>
#include <ctime>
#include <mutex>
#include <atomic>
#include <thread>
#include <iostream>
typedef time_t gtime_t;
typedef std::string srv_addr;
const std::string OPLOG_PATH = "master_oplog.log";
const std::string CHKPNT_PATH = "master_checkpoint.chkpnt";
const std::string del_prefix = "!";
#ifdef CUR
#undef CUR
#endif
#define CUR "master"
class Master
{
	template <typename Ret, typename ...Args>
	bool call(Ret &ret, srv_addr addr, std::string name, Args... args)
	{
		dbg << name << '@' << addr << std::endl;
		if(!cs_info_map[addr])
        {
            dbg << "panic:addr not found:"<<addr<<std::endl;
            return false;
        }
		LightDS::Service::RPCAddress rpc_addr = cs_info_map[addr]->addr;
//		LightDS::Service::RPCAddress adr;
//		adr.ip = addr;
//		adr.port = RPC_PORT;
		try
		{
			srv.RPCCall(rpc_addr, name, args...).get().convert(ret);
		}
		catch (LightDS::RPCException e)
		{
			dbg << "\tcatch RPCException: code:" << (int)e.code() << " what:" << e.what() << std::endl;
			return false;
		}
		return true;
	}

	template <typename Ret, typename ...Args>
	void bind(std::string rpcName, Ret(Master::*func)(Args...))
	{
		//dbg << rpcName << std::endl;
		srv.RPCBind(rpcName, std::function<Ret(Args...)>([this, func](Args ...args)->Ret {return (this->*func)(std::forward<Args>(args)...);}));
	}
#define BINDM(x) bind(#x, &Master::x);
//logger
	enum log_type
	{
	    op_file_create = 1, op_dir_create, op_remove, op_file_add_chunk, /*file_chmod,dir_chmod,(file_write, file_append)*/
	};
	int log_entry;
	std::ofstream flog;
	std::mutex log_lock;
	void flush_log()
	{
		dbg << "flushing" << std::endl;
		log_lock.lock();
		flog.close();
		log_entry = 0;
		flog.open(rootDir + OPLOG_PATH, std::ios::trunc);
		log_lock.unlock();
		save_checkpoint();
	}
	void oplog(log_type typ, std::string str)
	{
		if(!flog.is_open()) return;
		log_lock.lock();
		flog << (int)typ << ' ' << str << std::endl;
		dbg << log_entry << ' ' << (int)typ << ' ' << str << std::endl;
		++log_entry;
		log_lock.unlock();
		if (log_entry > FLUSH_THERSHOLD) flush_log();
	}
	GFSError load_replay_log()
	{
		GFSError err;
		flog.close();
		std::ifstream rlog(rootDir + OPLOG_PATH);
		if (!rlog.is_open()) gthrow(m_oplog_not_exist, OPLOG_PATH);
		while (1)
		{
			int typ;
			std::string str;
			rlog >> typ >> str;
			if (rlog.eof()) break;
			dbg << "type:" << (int)typ << ' ' << str << std::endl;
			log_type type = log_type(typ);
			switch(type)
			{
				case op_file_create:
					err = mk(str, false);
					if (!err.is_ok()) return err;
					break;
				case op_dir_create:
					err = mk(str, true);
					if (!err.is_ok()) return err;
					break;
				case op_remove:
					err = rm(str);
					if (!err.is_ok()) return err;
					break;
				case op_file_add_chunk:
				{
					size_t sep = str.find_last_of('#');
					std::string path = str.substr(0, sep);
					err = file_add_chunk(path, std::uint64_t(std::stoull(str.substr(sep + 1))));
					if (!err.is_ok()) return err;
					break;
				}
				default:
					gthrow(m_oplog_replay_error, str);
			}
		}
		GOK
	}
//end logger
//chunk manager
	struct m_chunk_meta
	{
		std::mutex lock;
		ChunkVersion ver;
		int rep_level;
		gtime_t expire;
		srv_addr primary;
		std::vector<srv_addr> rep; //convert address to int? //ip:port //behave like set
		m_chunk_meta(ChunkVersion nv): ver(nv), rep_level(REP_LEVEL), expire(0) {}
	};
	std::map<ChunkHandle, m_chunk_meta*> chunk_meta_map;
	std::map<ChunkHandle, bool> garbage_map;
	ChunkHandle new_ChunkHandle()
	{
		return (time(nullptr) << 32) | (int)((rand() % RAND_MAX) * (rand() % RAND_MAX));
	}
	GFSError chunk_create(ChunkHandle &nh)
	{
		GFSError err;
		do
		{
			nh = new_ChunkHandle();
		}
		while(chunk_meta_map[nh]);
		dbg << nh << std::endl;
		m_chunk_meta *nm = new m_chunk_meta(CHUNK_INIT_VER);
//		std::lock_guard <std::mutex>(nm->lock);
		std::vector<std::string> rep;
		choose_server(rep, nm->rep_level);
		chunk_meta_map[nh] = nm;
		for (auto i : rep)
		{
//			return srv.RPCCall(LightDS::Service::RPCAddress(i, uint16_t(11235)), "RPCCreateChunk", nh).get().as<GFSError>();
			bool t = call(err, i, "RPCCreateChunk", nh);
			if(t && err.is_ok())
			{
				nm->rep.push_back(i);
				server_add_rep(i, nh);
			}
		}
		if (!nm->rep.size()) gthrow(m_not_enough_servers, "");
		//chunk_meta_map[nh] = nm;
		//log?
		return err;
	}
	void chunk_recreate(ChunkHandle nh)
	{
        if (chunk_meta_map[nh]) return;
		m_chunk_meta *nm = new m_chunk_meta(CHUNK_INIT_VER);
		chunk_meta_map[nh] = nm;
	}
	GFSError rep_create(ChunkHandle handle, srv_addr address)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		std::lock_guard <std::mutex>(p->lock);
		for (auto it = p->rep.begin(); it != p->rep.end(); ++it)
		{
			if (*it == address)
			{
				gthrow(m_rep_exists, address);
			}
		}
		p->rep.push_back(address);
		//log?
		GOK
	}
	GFSError chunk_remove(ChunkHandle handle)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
//		for (auto it = p->rep.begin(); it != p->rep.end(); ++it)
//		{
//			//rpc call remove replica
//		}
		//log?
		delete p;
		chunk_meta_map.erase(handle);
		GOK
	}

	GFSError rep_remove(ChunkHandle handle, srv_addr address)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		std::lock_guard <std::mutex>(p->lock);
		for (auto it = p->rep.begin(); it != p->rep.end(); ++it)
		{
			if (*it == address)
			{
				p->rep.erase(it);
				GOK
				//log?
			}
		}
		gthrow(m_no_such_rep, address);
	}

	GFSError get_replica(std::vector<std::string> &vec, ChunkHandle handle)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		for (auto i : p->rep)
		{
//			if (!cs_info_map[i]->dead && std::chrono::seconds(time(nullptr) - cs_info_map[i]->last_alive) <= SERVER_KA_TIMEOUT) vec.push_back(i);
			if (!cs_info_map[i]->dead) vec.push_back(i);
		}
		GOK
	}
    GFSError get_replica_s(std::vector<std::string> &vec, ChunkHandle handle)
    {
        for (auto it=cs_info_map.begin();it!=cs_info_map.end();++it)
        {
            assert(it->second);
            it->second->lock.lock();
            if (!it->second->dead)
            {
                if(it->second->chunk_map[handle]) vec.push_back(it->first);
            }
            it->second->lock.unlock();
        }
        GOK
    }
	GFSError get_version(ChunkVersion &version, ChunkHandle handle)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		std::lock_guard <std::mutex>(p->lock);
		version = p-> ver;
		GOK
	}

	GFSError set_version(ChunkVersion version, ChunkHandle handle)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		std::lock_guard <std::mutex>(p->lock);
		p-> ver = version;
		GOK
	}

	GFSError extend_lease(ChunkVersion &ver, std::uint64_t &exp, ChunkHandle handle)
	{
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		std::lock_guard <std::mutex>(p->lock);
		exp = p -> expire = time(nullptr);
		ver = p -> ver = p -> ver + 1;
		GOK
	}

	GFSError get_primsec(srv_addr &prim, std::vector<srv_addr>&sec, std::uint64_t &exp, ChunkHandle handle)
	{
		GFSError err = EOK;
		m_chunk_meta* p = chunk_meta_map[handle];
		if (!p) gthrow(m_no_such_chunk, std::to_string(handle));
		std::lock_guard <std::mutex>(p->lock);
		sec = p -> rep;
        //get_replica_s(sec, handle);
		if (std::chrono::seconds(time(nullptr) - p -> expire) > LEASE_TIMEOUT)
		{
			dbg << "prim. expired renewing" << std::endl;
			//update version
			p -> ver = p -> ver + 1;
			//rpc call updateversion
			for (auto i : sec)
			{
				call(err, i, "RPCUpdateVersion", handle, p->ver);
//				err = srv.RPCCall(LightDS::Service::RPCAddress(i, uint16_t(11235)), "RPCUpdateVersion", handle, p->ver).get().as<GFSError>();
			}
			//grant lease
//			err = srv.RPCCall(LightDS::Service::RPCAddress(p->primary, uint16_t(11235)), "RPCGrantLease", handle, p->ver).get().as<GFSError>();
			p -> expire = time(nullptr);
			for (auto i : p->rep)
			{
				//p -> primary = p -> rep[rand() % (p -> rep.size())];
				p -> primary = i;
				dbg << "try granting lease:" << p->primary << std::endl;
				std::vector<std::tuple<ChunkHandle, ChunkVersion, std::uint64_t>> lease;
				lease.push_back(std::tuple<ChunkHandle, ChunkVersion, std::uint64_t>(handle, p->ver, p->expire));
				bool t = call(err, p->primary, "RPCGrantLease", lease);
				if (t && err.is_ok()) break;
			}
		}
		prim = p -> primary;
		exp = p -> expire;
		for (auto it = sec.begin(); it != sec.end(); ++it) //exclude primary from sec
		{
			if (*it == prim)
			{
				sec.erase(it);
				break;
			}
		}
		return err;
	}
//end chunk manager

//namespace
	struct rw_mutex
	{
		std::mutex mut;
		std::atomic_int rcnt;
		rw_mutex(): rcnt(0) {}
		void r_lock()
		{
			if(rcnt <= 0) mut.lock();
			++rcnt;
		}
		void r_unlock()
		{
			if(rcnt > 0)
			{
				--rcnt;
				if(!rcnt) mut.unlock();
			}
			else
			{
				dbg << "panic:r_unlock" << std::endl;
				exit(10);
			}
		}
		void lock()
		{
			mut.lock();
			if(!rcnt) --rcnt;
		}
		void unlock()
		{
			if(rcnt == -1) rcnt = 0, mut.unlock();
			else
			{
				dbg << "panic:nlock" << std::endl;
				exit(11);
			}
		}
	};
	struct ns_node
	{
		std::string name;//?
		std::map<std::string, ns_node*> child;
//		ns_node *par;
		bool is_dir;
		rw_mutex mut;
		std::uint64_t len;
		std::vector<ChunkHandle> chunk_list;
		ns_node(std::string n, bool d): name(n), is_dir(d), len(0) {}
	};

	ns_node *root;

//	GFSError recursive_do(int i, ns_node *p, std::string &path, (GFSError)(*f)())
//	{
//		int l = path.length();
//		std::string tmp;
//		if (path[i] == '/') ++i;
//		for (; i < l && path[i] != '/'; ++i)
//		{
//			tmp += path[i];
//		}
//		ns_node* t = p -> child[tmp];
//		if (!t) gthrow(m_no_such_file, path);
//		if (i == l)
//		{
//			return f(
//		}
//		else
//		{
//			p = t;
//		}
////		if (tmp.back() == '/')
//		return p;
//	}
	struct locket
	{
		ns_node *p, *t;
		int r;
		std::vector<ns_node*> lock_vec;
		std::string last;
		locket(std::string &path, int rd, ns_node *rt): p(rt), t(nullptr), r(-1)
		{
			assert(path[0] == '/');
			int i = 1, l = path.length();
			while (i < l)
			{
				last.clear();
				for (; i < l && path[i] != '/'; ++i)
				{
					last += path[i];
				}
				if (i < l && path[i] == '/') ++i;
//				dbg << i << ' ' << last << std::endl;
				p->mut.r_lock();
				lock_vec.push_back(p);
				t = p->child[last];
				if (i >= l) break;
				p = t;
//				dbg << p << std::endl;
				if (!p) return;
			}
			if (last.back() == '/') last.pop_back();
			if (!t) return;
			if (rd) t->mut.r_lock();
			else t->mut.lock();
			r = rd;
			lock_vec.push_back(t);
		}
		~locket()
		{
			if (r == 1)
			{
				lock_vec[lock_vec.size() - 1]->mut.r_unlock();
				lock_vec.pop_back();
			}
			else if (r == 0)
			{
				lock_vec[lock_vec.size() - 1]->mut.unlock();
				lock_vec.pop_back();
			}
			for (int i = lock_vec.size() - 1; i >= 0; --i)
			{
				lock_vec[i]->mut.r_unlock();
			}
		}
	};
//	ns_node* get_parent_dir()
//	{
//		ns_node *p = root;
//		assert(path[0] == '/');
//		int i = 1, l = path.length();
//		std::string tmp;
//		while (i < l)
//		{
//			tmp.clear();
//			for (; i < l && path[i] != '/'; ++i)
//			{
//				tmp += path[i];
//			}
//			if (i == l) break;
//			p->mut.r_lock();
//			lock_vec.push_back(p);
//			p = p->child[tmp];
//			if (!p) return nullptr;
//		}
//		if (tmp.back() == '/') tmp.pop_back();
//		else p->mut.lock();
//		path = tmp;
//		return p;
//	}
	void remove_tree(ns_node *t)
	{
		for (auto it = t->child.begin(); it != t->child.end(); ++it)
		{
			dbg << it->first << ' ' << it->second << std::endl;
			if (it->second) remove_tree(it->second);
		}
		delete t;
	}
	GFSError rm(std::string &path)
	{
		locket lck(path, 0, root);
		std::string last = lck.last;
		ns_node *p = lck.p, *t = lck.t;
		dbg << path << ' ' << last << ' ' << p << ' ' << t << std::endl;
		if (!p || !t) gthrow(m_no_such_file, path);
		if(t->is_dir)
		{
			if (!t->child.empty()) gthrow(m_dir_not_empty, path);
			delete t;
			p->child.erase(last);
		}
		else
		{
			std::string prefix = del_prefix + std::to_string(time(nullptr)) + '_';
			t->name.insert(0, prefix);
			p->child.erase(last);
//			p->child[path] = nullptr;
			p->child[t->name] = t;
		}
		oplog(op_remove, path);
		GOK
	}
//	GFSError rmdir(ns_node *p, ns_node *t, const std::string &path)
//	{
//		if (!t->child.empty()) gthrow(m_dir_not_empty, path);
//		delete t;
//		p->child.erase(path);
////		p->child[path] = nullptr;
//		//remove empty dir
//		GOK
//	}
//	GFSError rmfile(ns_node *p, ns_node *t, const std::string &path)
//	{
//		std::string prefix = del_prefix + std::to_string(time(nullptr)) + '_';
//		t->name.insert(0, prefix);
//		p->child.erase(path);
////		p->child[path] = nullptr;
//		p->child[t->name] = t;
//		//rename deleted file
//		GOK
//	}
	GFSError mk(std::string &path, bool is_dir)
	{
		locket lck(path, 0, root);
		std::string last = lck.last;
		ns_node *p = lck.p, *t = lck.t;
		dbg << path << ' ' << last << ' ' << is_dir << ' ' << p << ' ' << t << std::endl;
		if (!p) gthrow(m_no_such_file, path);
		if (t) gthrow(m_file_exists, path);
		if (last.empty()) gthrow(m_invalid_filename, path);
		ns_node *n = new ns_node(last, is_dir);
		p->child[last] = n;
//		dbg << n << ' ' << root->child[path] << std::endl;
		oplog((is_dir ? op_dir_create : op_file_create), path);
		GOK
	}
	GFSError get_file_list(std::vector<std::string> &vec, std::string &path)
	{
		locket lck(path, 1, root);
		ns_node *p = lck.p, *t = lck.t;
		if (!p || !t) gthrow(m_no_such_file, path);
		if (!t->is_dir) gthrow(m_is_a_file, path);
		for (auto it = t->child.begin(); it != t->child.end(); ++it)
		{
			assert(it->second);
			it->second->mut.r_lock();
			vec.push_back(it->second->name);
//			vec.push_back(it->first);
			it->second->mut.r_unlock();
		}
		GOK
	}
	GFSError get_chunk_handle(ChunkHandle &handle, std::string &path, std::uint64_t chunkIndex)
	{
		locket lck(path, 1, root);
		ns_node *p = lck.p, *t = lck.t;
		if (!p || !t) gthrow(m_no_such_file, path);
		if (t->is_dir) gthrow(m_is_a_dir, path);
		if (t->chunk_list.size() < chunkIndex) gthrow(m_invalid_index, path + '[' + std::to_string(chunkIndex) + ']');
		//alt: create chunk
		if (t->chunk_list.size() == chunkIndex)
		{
			ChunkHandle nh;
			chunk_create(nh);
			t->chunk_list.push_back(nh);
			dbg << "new chunk at:" << path << " H:" << nh << " idx:" << chunkIndex << std::endl;
			oplog(op_file_add_chunk, path + '#' + std::to_string(nh));
		}
		handle = t->chunk_list[chunkIndex];
		GOK
	}
	GFSError get_file_info(bool &is_dir, std::uint64_t &len, std::uint64_t &chunk, std::string &path)
	{
		locket lck(path, 1, root);
		ns_node *p = lck.p, *t = lck.t;
		if (!p || !t) gthrow(m_no_such_file, path);
		is_dir = t->is_dir;
		len = is_dir ? 0 : t->len;
		chunk = is_dir ? 0 : t->chunk_list.size();
		GOK
	}
	GFSError file_add_chunk(std::string &path, ChunkHandle handle)
	{
		locket lck(path, 0, root);
		ns_node *p = lck.p, *t = lck.t;
		if (!p || !t) gthrow(m_no_such_file, path);
		for (auto i : t->chunk_list)
		{
			if (i == handle) gthrow(m_chunk_exists, path + std::to_string(handle));
		}
		t->chunk_list.push_back(handle);
        chunk_recreate(handle);
		dbg << path << " H:" << handle << std::endl;
		GOK
	}
	void get_garbage(std::vector<ChunkHandle> &gar)
	{
		get_garbage(gar, root);
	}
	void get_garbage(std::vector<ChunkHandle> &gar, ns_node *t)
	{
		for (auto it = t->child.begin(); it != t->child.end(); ++it)
		{
			if (it->second->is_dir)
			{
				get_garbage(gar, it->second);
			}
			else if (it->second->name.substr(0, 1) == del_prefix)
			{
				int p = 1;
				while (it->second->name[p] != '_') ++p;
				gtime_t t = std::stoi(it->second->name.substr(1, p - 1));
				if (GC_INTERVAL < std::chrono::seconds(time(nullptr) - t))
				{
					gar.insert(gar.begin(), it->second->chunk_list.begin(), it->second->chunk_list.end());
				}
			}
		}
	}
	struct chk_node
	{
		bool is_dir;
		std::map<std::string, int> child;
		std::vector<ChunkHandle> chunks;
	};
	void save_node(ns_node *t, std::vector<chk_node> &vec)
	{
		assert(t);
		chk_node node;
		node.is_dir = t->is_dir;
		if (t->is_dir)
		{
			for (auto it = t->child.begin(); it != t->child.end(); ++it)
			{
				assert(it->second);
				save_node(it->second, vec);
				node.child[it->first] = vec.size() - 1;
			}
		}
		else
		{
			node.chunks = t->chunk_list;
		}
		vec.push_back(node);
	}
	void load_node(ns_node *t, std::vector<chk_node> &vec, int idx)
	{
		assert(t);
		if (vec[idx].is_dir)
		{
			for (auto it = vec[idx].child.begin(); it != vec[idx].child.end(); ++it)
			{
				load_node(t, vec, it->second);
				//node.child[it->first] = vec.size() - 1;
			}
		}
		else
		{
			//node.chunks = t->chunk_list;
		}

	}
	void save_checkpoint()
	{
		std::ofstream fout(rootDir + CHKPNT_PATH, std::ios::trunc);
		std::vector<chk_node> vec;
		save_node(root, vec);
	}
	void load_checkpoint()
	{
		std::ifstream fin(rootDir + CHKPNT_PATH);
		if (!fin.is_open())
		{
			dbg << "load checkpoint failed" << std::endl;
		}
		std::vector<chk_node> vec;
		//fin >>
	}
//end namespace

//chunkserver manager
	//enum chunk_status
	//{
	//ch_ne = 0, ch_good, ch_stale, ch_failed
	//};
	struct cs_info
	{
		LightDS::Service::RPCAddress addr;
		std::mutex lock;
		int dead;
		gtime_t last_alive;
		std::map<ChunkHandle, bool> chunk_map;
		cs_info(): dead(0), last_alive(time(nullptr)) {}
	};
	std::map<srv_addr, cs_info*> cs_info_map;
	GFSError server_add_rep(srv_addr address, ChunkHandle handle)
	{
		cs_info *t = cs_info_map[address];
		if (!t) gthrow(m_no_such_server, address);
		std::lock_guard <std::mutex>(t->lock);
		t->chunk_map[handle] = 1;
		GOK
	}
	GFSError server_remove_rep(srv_addr address, ChunkHandle handle)
	{
		cs_info *t = cs_info_map[address];
		if (!t) gthrow(m_no_such_server, address);
		std::lock_guard <std::mutex>(t->lock);
		t->chunk_map[handle] = 0;
//		t->chunk_map.erase(handle);
		GOK
	}
	GFSError add_server(srv_addr address)
	{
		dbg << address << std::endl;
		if (cs_info_map[address]) gthrow(m_server_exists, address);
		//std::vector<LightDS::Service::RPCAddress> vec = srv.ListService("chunkserver");
		//for (auto i : vec)
		//{
			//if (i.ip == address)
			//{
				//cs_info *t = new cs_info();
				//cs_info_map[address] = t;
				//t->addr = i;
				//GOK
			//}
		//}
        cs_info *t = new cs_info();
	    cs_info_map[address] = t;
        t->addr.ip = address;
        t->addr.port = 7778;
	    GOK
	    gthrow(m_server_not_found, address);
	}
	GFSError remove_server(srv_addr address)
	{
		cs_info *t = cs_info_map[address];
		if (!t) gthrow(m_no_such_server, address);
		//std::lock_guard <std::mutex>(t->lock);
		delete t;
		cs_info_map[address] = nullptr;
		cs_info_map.erase(address);
		GOK
	}
	GFSError choose_rep_server(std::set<srv_addr> &rep, unsigned int num)
	{
		std::vector<srv_addr> ret;
		for (auto it = cs_info_map.begin(); it != cs_info_map.end(); ++it)
		{
			if (!it->second)
			{
				dbg << "oops:" << it->first << std::endl;
				continue;
			}
			if (!it->second->dead && rep.find(it->first) == rep.end())
			{
				ret.push_back(it->first);
			}
		}
		rep.clear();
		if (ret.size() < num)
		{
			for (auto i : ret) rep.insert(i);
			gthrow(m_not_enough_servers, std::to_string(num));
		}
		for (unsigned int i = 0; i < num; ++i)
		{
			rep.insert(ret[i]);
		}
		GOK
	}
	GFSError choose_server(std::vector<srv_addr> &rep, unsigned int num)
	{
		for (auto it = cs_info_map.begin(); it != cs_info_map.end(); ++it)
		{
			if (!it->second)
			{
				dbg << "oops:" << it->first << std::endl;
				continue;
			}
			if (!it->second->dead)
			{
				rep.push_back(it->first);
			}
		}
		if (rep.size() < num) gthrow(m_not_enough_servers, std::to_string(num));
		for (unsigned int i = 0; i < rep.size() - num; ++i)
		{
			rep.pop_back();
		}
		GOK
	}
	GFSError server_keep_alive(srv_addr address)
	{
		cs_info *t = cs_info_map[address];
		if (!t)
		{
			//re-register server?
			add_server(address);
			dbg << "server registered " << address << std::endl;
		}
		else
		{
			std::lock_guard <std::mutex>(t->lock);
			dbg << address << std::endl;
			t->last_alive = time(nullptr);
			if (t->dead)
			{
				t->dead = 0;
				dbg << "server revived " << address << std::endl;
			}
		}
		GOK
	}
	GFSError scan_dead_sever()
	{
		for (auto it = cs_info_map.begin(); it != cs_info_map.end();)
		{
			if (!it->second)
			{
				dbg << "oops:" << it->first << std::endl;
                ++it;
				continue;
			}
			gtime_t now = time(nullptr);
			it->second->lock.lock();
			dbg << it->first << " last_alive:" << it->second->last_alive << " dead:" << it->second->dead << std::endl;
			if (std::chrono::seconds(now - it->second->last_alive) > 2 * SERVER_KA_TIMEOUT)
			{
				//dead
				dbg << "dead server:" << it->first << std::endl;
				it->second->dead = 1;
				for (auto cit = it->second->chunk_map.begin(); cit != it->second->chunk_map.end(); ++cit)
				{
					if (cit->second)
					{
						dbg << "delete rep:" << cit->first << std::endl;
						rep_remove(cit->first, it->first);
					}
				}
				it->second->lock.unlock();
				delete it->second;
				it->second = nullptr;
				cs_info_map.erase(it++);
				//remove_server(it->first);
				continue;
			}
			else if (!it->second->dead && std::chrono::seconds(now - it->second->last_alive) > SERVER_KA_TIMEOUT)
			{
				it->second->dead = 1;
				dbg << "timeout server" << it->first << std::endl;
			}
			it->second->lock.unlock();
			++it;
		}
		GOK
	}
//end chunkserver manager

	void re_replication()
	{
		for (auto it = chunk_meta_map.begin(); it != chunk_meta_map.end(); ++it)
		{
            if(!it->second || it->second->rep.empty()) continue;
			it -> second -> lock.lock();
            //std::vector<std::string> rep;
            //get_replica_s(rep, it->first);
			int t = it->second->rep_level - it->second->rep.size();
			if (t > 0)
			{
				std::set<srv_addr>arep;
				for (auto i : it->second->rep) arep.insert(i);
				GFSError err = choose_rep_server(arep, t);
				dbg << "REP:" << it->first <<" tgt:"<<t<<" rep:"<<it->second->rep.size()<<" avail:"<<arep.size()<< std::endl;
				if (!err.is_ok())
				{
					dbg << "not enough targets:" << arep.size() << '/' << t << ' ' << it->first << std::endl;
					//continue;
				}
                for (auto i : arep)
				{
                    for (auto j : it->second->rep)
					{
						bool r = call(err, j, "RPCSendCopy", it->first, cs_info_map[i]->addr.to_string());
						if (r && err.is_ok())
						{
							server_add_rep(i, it->first);
							it->second->rep.push_back(i);
							break;
						}
					}
				}
			}
			it -> second -> lock.unlock();
		}
	}
public:
	Master(LightDS::Service &srv, const std::string &rootDir): srv(srv), rootDir(rootDir), up(0), log_entry(0)
	{
		BINDM(RPCHeartbeat)
		BINDM(RPCGetPrimaryAndSecondaries)
		BINDM(RPCGetReplicas)
		BINDM(RPCGetFileInfo)
		BINDM(RPCCreateFile)
		BINDM(RPCDeleteFile)
		BINDM(RPCMkdir)
		BINDM(RPCListFile)
		BINDM(RPCGetChunkHandle)

		root = new ns_node("/", true);
		dbg << "root:" << root << std::endl;
		srand(time(nullptr));
	}
	~Master()
	{
		for (auto it = chunk_meta_map.begin(); it != chunk_meta_map.end(); ++it)
		{
			if (it->second) delete it->second;
		}
		chunk_meta_map.clear();
		for (auto it = cs_info_map.begin(); it != cs_info_map.end(); ++it)
		{
			if (it->second) delete it->second;
		}
		cs_info_map.clear();
		garbage_map.clear();
		remove_tree(root);
		root = nullptr;
	}
	void ls_all(ns_node *t, int d)
	{
		for (auto it = t->child.begin(); it != t->child.end(); ++it)
		{
			ns_node *x = it->second;
			if (x)
			{
				for (int i = 0; i < d; ++i) std::cerr << '\t';
				std::cerr << it->first << std::endl;
				if (x->is_dir) ls_all(x, d + 1);
			}
		}
	}
	void Start()
	{
		dbg << "Master starting" << std::endl;
		up = 1;
		load_replay_log();
		flog.open(rootDir + OPLOG_PATH, std::ios::trunc);
		std::thread p(&Master::BackgroundActivity, this);
		p.detach();
		dbg << "Master started" << std::endl;
	}
	void Shutdown()
	{
		std::lock_guard<std::mutex> lock(mtx);
		up = 0;
		flog.close();
		cv.notify_all();
		dbg << "Master shutdown" << std::endl;
	}

public:
// BackgroundActivity does all the background activities:
// dead chunkserver handling, garbage collection, stale replica detection, etc
	void BackgroundActivity()
	{
		int i = 0;
		std::unique_lock<std::mutex> lock(mtx);
		for (; up; cv.wait_for(lock, BG_INTERVAL))
		{
			dbg << "BG work " << i << std::endl;
			//dead srv
			dbg << "scan dead server" << std::endl;
			scan_dead_sever();
			//garbage
			std::vector<ChunkHandle> gar;
			get_garbage(gar);
			dbg << "get garbage:" << gar.size() << std::endl;
			for (auto i : gar) garbage_map[i] = true;
			//for (auto it = gar.begin(); it != gar.end(); ++it) chunk_remove(*it);
			//stale
			//re-replication
			dbg << "re-rep" << std::endl;
			re_replication();
			//std::this_thread::sleep_for(std::chrono::seconds(BG_INTERVAL));
			dbg << "BG done " << i++ << std::endl;
		}
	}

// RPCHeartbeat is called by chunkserver to let the master know that a chunkserver is alive.
	std::tuple < GFSError, std::vector<ChunkHandle> /*Garbage Chunks*/ >
	RPCHeartbeat(std::vector<ChunkHandle> leaseExtensions, std::vector<std::tuple<ChunkHandle, ChunkVersion>> chunks, std::vector<ChunkHandle> failedChunks)
	{
		srv_addr address = srv.getRPCCaller();
		dbg << "from:" << address << " chunks:" << chunks.size() << " failed:" << failedChunks.size() << std::endl;
		server_keep_alive(address);
		GFSError err = EOK;
		std::vector<ChunkHandle> gar;
        
        cs_info_map[address]->chunk_map.clear();
		for (auto it = chunks.begin(); it != chunks.end(); ++it)
		{
			ChunkHandle handle = std::get<0>(*it);
			if (garbage_map[handle])
			{
				dbg << "gar:" << handle << std::endl;
				gar.push_back(handle);
				continue;
			}
			ChunkVersion ver = 0, cver = std::get<1>(*it);
			err = get_version(ver, handle);
			if (!err.is_ok())
			{
				// add to master
				dbg << "unknown chunk copy:" << handle << std::endl;
				chunk_recreate(handle);
				rep_create(handle, address);
				server_add_rep(address, handle);
				continue;
			}
			if (ver > cver)
			{
				//stale
				dbg << "stale:" << handle << ' ' << ver << ' ' << cver << std::endl;
				gar.push_back(handle);
				rep_remove(handle, address);
				server_remove_rep(address, handle);
				continue;
			}
			else if (ver < cver)
			{
				//update version
				dbg << "newer:" << handle << ' ' << ver << ' ' << cver << std::endl;
				err = set_version(cver, handle);
                //send copy
			}
			rep_create(handle, address);
			server_add_rep(address, handle);
		}
		for (auto it = failedChunks.begin(); it != failedChunks.end(); ++it)
		{
			err = rep_remove(*it, address);
			server_remove_rep(address, *it);
		}
		std::vector<std::tuple<ChunkHandle, ChunkVersion, std::uint64_t>> vec;
		for (auto it = leaseExtensions.begin(); it != leaseExtensions.end(); ++it)
		{
			ChunkVersion ver = 0;
			std::uint64_t exp = 0;
			err = extend_lease(ver, exp, *it);
			if (err.is_ok())
			{
				vec.push_back(std::tuple<ChunkHandle, ChunkVersion, std::uint64_t>(*it, ver, exp));
			}
		}
		call(err, address, "RPCGrantLease", vec);
		dbg << "garbage:" << gar.size() << std::endl;
		return std::tuple <GFSError, std::vector<ChunkHandle>>(err, gar);
	}

// RPCGetPrimaryAndSecondaries returns lease holder and secondaries of a chunk.
// If no one holds the lease currently, grant one.
	std::tuple < GFSError, std::string /*Primary Address*/, std::vector<std::string> /*Secondary Addresses*/, std::uint64_t /*Expire Timestamp*/ >
	RPCGetPrimaryAndSecondaries(ChunkHandle handle)
	{
		std::string prim;
		std::vector<std::string> sec;
		std::uint64_t exp;
		GFSError err = get_primsec(prim, sec, exp, handle);
		assert(cs_info_map[prim]);
		prim = cs_info_map[prim]->addr.to_string();
		dbg << "prim:" << prim << " exp:" << exp << " handle:" << handle << " sec:";
		for (auto & i : sec)
		{
			assert(cs_info_map[i]);
			i = cs_info_map[i]->addr.to_string();
			dbga << i << '\t';
		}
		dbga << std::endl;
		return std::tuple<GFSError, std::string, std::vector<std::string>, std::uint64_t>(err, prim, sec, exp);
	}

// RPCGetReplicas is called by client to find all chunkservers that hold the chunk.
	std::tuple < GFSError, std::vector<std::string> /*Locations*/ >
	RPCGetReplicas(ChunkHandle handle)
	{
		std::vector<std::string> vec;
		std::vector<std::string> ret;
		GFSError err = get_replica(vec, handle);
		dbg << "rep:";
		for (auto & i : vec)
		{
			assert(cs_info_map[i]);
			ret.push_back(cs_info_map[i]->addr.to_string());
			dbga << i << '\t';
		}
		dbga << std::endl;
		return std::tuple <GFSError, std::vector<std::string>>(err, ret);
	}

// RPCGetFileInfo is called by client to get file information
	std::tuple < GFSError, bool /*IsDir*/, std::uint64_t /*Length*/, std::uint64_t /*Chunks*/ >
	RPCGetFileInfo(std::string path)
	{
		bool is_dir;
		std::uint64_t len = 0, chunk = 0;
		GFSError err = get_file_info(is_dir, len, chunk, path);
		dbg << path << " dir:" << is_dir << " len:" << len << " chunks:" << chunk << std::endl;
		return std::tuple <GFSError, bool, std::uint64_t, std::uint64_t>(err, is_dir, len, chunk);
	}

// RPCCreateFile is called by client to create a new file
	GFSError
	RPCCreateFile(std::string path)
	{
		return mk(path, false);
		//create chunk?
	}

// RPCCreateFile is called by client to delete a file
	GFSError
	RPCDeleteFile(std::string path)
	{
		return rm(path);
	}

// RPCMkdir is called by client to make a new directory
	GFSError
	RPCMkdir(std::string path)
	{
		return mk(path, true);
	}

// RPCListFile is called by client to get the file list
	std::tuple < GFSError, std::vector<std::string> /*FileNames*/ >
	RPCListFile(std::string path)
	{
		std::vector<std::string> vec;
		GFSError err = get_file_list(vec, path);
		return std::tuple <GFSError, std::vector<std::string>>(err, vec);
	}

// RPCGetChunkHandle returns the chunk handle of (path, index).
// If the requested index is larger than the number of chunks of this path by exactly one, create one.
	std::tuple<GFSError, ChunkHandle>
	RPCGetChunkHandle(std::string path, std::uint64_t chunkIndex)
	{
		ChunkHandle handle;
		GFSError err = get_chunk_handle(handle, path, chunkIndex);
		return std::tuple <GFSError, ChunkHandle>(err, handle);
	}

protected:
	LightDS::Service &srv;
	std::string rootDir;
	int up;
	std::mutex mtx;
	std::condition_variable cv;
};

#endif
