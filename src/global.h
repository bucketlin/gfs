#ifndef GFS_GLOBAL_H
#define GFS_GLOBAL_H
#include <chrono>

const long long CHUNK_INIT_VER = 1;
const int FLUSH_THERSHOLD = 1000;
const int RPC_REDIAL = 10;

#define DEBUGP 1
#if DEBUGP
const int CHUNK_SIZE = 64 << 20; //64MB
const int REP_LEVEL = 3;
const std::chrono::seconds SERVER_KA_TIMEOUT(20);
const std::chrono::seconds LEASE_TIMEOUT(600);
const std::chrono::hours GC_INTERVAL(1);
const std::chrono::seconds HB_INTERVAL(7);
const std::chrono::seconds BG_INTERVAL(5);
#else
const int CHUNK_SIZE = 64 << 20; //64MB
const int REP_LEVEL = 3;
const std::chrono::seconds SERVER_KA_TIMEOUT(30);
const std::chrono::seconds LEASE_TIMEOUT(10);
const std::chrono::seconds GC_INTERVAL(240);
const std::chrono::seconds HB_INTERVAL(3);
const std::chrono::seconds BG_INTERVAL(5);
#endif

#endif
