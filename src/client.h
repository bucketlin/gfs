#ifndef GFS_CLIENT_H
#define GFS_CLIENT_H

#include "common.h"
#include "global.h"
#include <user.hpp>
#include <ctime>

#ifdef CUR
#undef CUR
#endif
#define CUR "client"
class Client
{
	template <typename Ret, typename ...Args>
	bool call(Ret &ret, LightDS::User::RPCAddress addr, std::string name, Args... args)
	{
		dbg << name << '@' << addr.ip << std::endl;
        try{
            srv.RPCCall(addr, name, args...).get().convert(ret);
        }catch(LightDS::RPCException e){
            dbg<<"call master failed:"<<(int)e.code()<<' '<<e.what()<<std::endl;
	    return false;
        }
	return true;
	}
	template <typename Ret, typename ...Args>
	bool call(Ret &ret, std::string service, std::string name, Args... args)
	{
		dbg << name << '@' << service << std::endl;
		std::vector<LightDS::User::RPCAddress> vec=srv.ListService(service);
		assert(vec.size());
        try{
            srv.RPCCall(vec[0], name, args...).get().convert(ret);
        }catch(LightDS::RPCException e) {
            dbg<<"call chunkserver failed"<<(int)e.code()<<' '<<e.what()<<std::endl;
	    return false;
        }
	return true;
	}

//namespace cache
	const time_t chunk_meta_expire = 10;
	struct chunk_meta
	{
		bool enabled;
		time_t expire;
		ChunkHandle handle;
		ChunkVersion version;
	};
	std::map<std::pair<std::string, int>, chunk_meta> chunk_cache;
//replica cache
	const time_t rep_meta_expire = 10;
	struct rep_meta
	{
		bool enabled;
		time_t expire;
		std::vector<std::string> rep;
	};
	std::map<ChunkHandle, rep_meta> rep_cache;	

public:
	Client(LightDS::User &srv): srv(srv)
	{
		srv.Run();
	}

public:
	// Create creates a new file on the specific path on GFS.
	GFSError
	Create(const std::string &path)
	{
		GFSError ret;
		call(ret, "master", "RPCCreateFile", path);
		return ret;
	}

	// Mkdir creates a new directory on GFS.
	GFSError
	Mkdir(const std::string & path)
	{
		GFSError ret;
		call(ret, "master", "RPCMkdir", path);
		return ret;
	}

	// List lists files and directories in specific directory on GFS.
	std::tuple < GFSError, std::vector<std::string> /*filenames*/ >
	List(const std::string & path)
	{
		std::tuple <GFSError, std::vector<std::string>> ret;
		call(ret, "master", "RPCListFile", path);
		return ret;
	}

	// Read reads the file at specific offset.
	// It reads up to data.size() bytes form the File.
	// It return the number of bytes, and an error if any.
	std::tuple < GFSError, size_t /*byteOfRead*/ >
    Read(const std::string & path, std::uint64_t offset, std::vector<char> &data) {
        std::vector<char> _data;
        size_t tmp = 0;
        std::tuple < GFSError, size_t > ret;
        int idx = offset / CHUNK_SIZE;
        std::tuple<GFSError, ChunkHandle> t = GetChunkHandle(path, idx);
        
        if(!std::get<0>(t).is_ok()) {
            return std::tuple<GFSError, size_t>(std::get<0>(t), 0);
        }

        std::uint64_t inoffset = offset % CHUNK_SIZE;
        int length = data.size();
	dbg << path << " off:"<<offset<< " idx:"<<idx<<" len:"<<length<<std::endl;
        while(length > 0) {
            dbg<<idx<<" "<<std::endl;
            std::vector<char> part;
            if(length + inoffset > CHUNK_SIZE){
                part.resize(CHUNK_SIZE - inoffset);
            }
            else {
                part.resize(length);
            }
            std::tuple < GFSError, size_t > ret = ReadChunk(std::get<1>(t), inoffset, part);
            tmp += std::get<1>(ret);
	    dbg<<" total:"<<tmp<<" idx:"<<idx<<" H:"<<std::get<1>(t)<<" inoff:"<<inoffset<<" read:"<<part.size()<<std::endl; 
            length -= (CHUNK_SIZE - inoffset);
            for(unsigned int i = 0; i < part.size(); ++i) {
                _data.push_back(part[i]);
            }

	    if(!std::get<0>(ret).is_ok()) {
		dbg<<"read chunk error:"<<idx<<std::endl;
                //return std::tuple<GFSError, size_t>(std::get<0>(ret), tmp);
                //return std::tuple<GFSError, size_t>(EOK, tmp);
            break;
            }
            
	    if (length <= 0) break;
            ++idx;
            t = GetChunkHandle(path, idx);
            if(!std::get<0>(t).is_ok()) {
                return std::tuple<GFSError, size_t>(std::get<0>(t), 0);
            }
            inoffset = 0;
        }
        data.resize(_data.size());
        for(unsigned int i = 0; i < _data.size(); ++i){
            data[i] = _data[i];
        }
        return std::tuple < GFSError, size_t > (GFSError(), tmp);
    }

	// Write writes data to the file at specific offset.
	GFSError
	Write(const std::string & path, std::uint64_t offset, const std::vector<char> &data)
	{
        size_t tmp = 0;
        std::tuple < GFSError, size_t > ret;
        int idx = offset / CHUNK_SIZE;
        std::tuple<GFSError, ChunkHandle> t = GetChunkHandle(path, idx);
        
        if(!std::get<0>(t).is_ok()) {
            return std::get<0>(t);
        }
        
        std::uint64_t inoffset = offset % CHUNK_SIZE;
        int length = data.size();
	dbg << path << " off:"<<offset<< " idx:"<<idx<<" len:"<<length<<std::endl;
        while(length > 0) {
            
            std::vector<char> part;
            if(length + inoffset > CHUNK_SIZE){
                part.resize(CHUNK_SIZE - inoffset);
            }
            else {
                part.resize(length);
            }
            for(unsigned int i = 0; i < part.size(); ++i) {
                part[i] = data[i + tmp];
            }
            
            GFSError ret = WriteChunk(std::get<1>(t), inoffset, part);
            if(!ret.is_ok()) {
                return ret;
            }
            tmp += part.size();
            length -= (CHUNK_SIZE - inoffset);
	    dbg<<" total:"<<tmp<<" idx:"<<idx<<" H:"<<std::get<1>(t)<<" inoff:"<<inoffset<<"written:"<<part.size()<<std::endl; 
	    if (length <= 0) break;
            
            ++idx;
            t = GetChunkHandle(path, idx);
            if(!std::get<0>(t).is_ok()) {
                return std::get<0>(t);
            }
            inoffset = 0;
        }
        GOK;
	}

	// Append appends data to the file. Offset of the beginning of appended data is returned.
	std::tuple < GFSError, std::uint64_t /*offset*/ >
    Append(const std::string & path, const std::vector<char> &data){
        std::tuple < GFSError, bool , std::uint64_t /*Length*/, std::uint64_t /*Chunks*/ > tmp;
        call(tmp, "master", "RPCGetFileInfo", path);
        if(!std::get<0>(tmp).is_ok()) return std::tuple< GFSError, std::uint64_t > (std::get<0>(tmp), 0);
        std::tuple < GFSError, size_t > ret;
//        std::uint64_t offset = std::get<2>(tmp);
//        int idx = offset / CHUNK_SIZE;
        std::uint64_t chunks = std::get<3>(tmp);
	chunks = chunks ? chunks - 1 : 0;
        std::tuple<GFSError, ChunkHandle> t = GetChunkHandle(path, chunks);
        if(!std::get<0>(t).is_ok()) {
            return std::tuple<GFSError, std::uint64_t>(std::get<0>(t), 0);
        }
	GFSError err;
	std::uint64_t off;
	std::tie(err, off) = AppendChunk(std::get<1>(t), data);
        if(err.errCode == GFSErrorCode::s_chunk_filled_up){
            std::tuple<GFSError, ChunkHandle> t = GetChunkHandle(path, ++chunks);
            if(!std::get<0>(t).is_ok()) {
                return std::tuple<GFSError, std::uint64_t>(std::get<0>(t), 0);
            }
//            GFSError err;
//            std::uint64_t off;
            std::tie(err, off) = AppendChunk(std::get<1>(t), data);
	    dbg << "padded" << std::endl;
        }
	off += chunks * CHUNK_SIZE;
	dbg << path << " ofs:" << off << " idx:" << chunks <<" H:"<<std::get<1>(t)<< " len:" << data.size() << std::endl;
        return std::make_tuple(err, off);
    }

public:
	// GetChunkHandle returns the chunk handle of (path, index).
	// If the chunk doesn't exist, create one.
	std::tuple<GFSError, ChunkHandle>
	GetChunkHandle(const std::string & path, std::uint64_t index)
	{
		std::tuple<GFSError, ChunkHandle> ret;
		call(ret, "master", "RPCGetChunkHandle", path, index);
		return ret;
	}

	// ReadChunk reads data from the chunk at specific offset.
	// data.size()+offset  should be within chunk size.
	std::tuple < GFSError, size_t /*byteOfRead*/ >
	ReadChunk(ChunkHandle handle, std::uint64_t offset, std::vector<char> &data)
	{
		std::tuple < GFSError, std::vector<std::string> > ret;
        	call(ret, "master", "RPCGetReplicas", handle);
		GFSError err = std::get<0>(ret);
		if (!err.is_ok()) return std::tuple <GFSError, size_t>(err, 0);
		auto vec = std::get<1>(ret);
	        assert(vec.size());
		dbg<<"rep sz:"<<vec.size()<<std::endl;
		for (auto i:vec)
		{
			dbg << "reading "<< i <<std::endl;
			std::tuple <GFSError, std::string> t;
			bool r = call(t, LightDS::User::RPCAddress::from_string(i), "RPCReadChunk", handle, offset, data.size());
			GFSError err = std::get<0>(t);
			if (!(r && err.is_ok())) continue;
			std::string &str = std::get<1>(t);
			dbg << "expect:"<<data.size()<<" read:"<<str.size() <<std::endl;
			if (data.size() != str.size()) continue;
			data.assign(str.begin(), str.end());
            return std::tuple <GFSError, size_t>(err, data.size());
		}
		return std::tuple <GFSError, size_t>(err, data.size());
	}

	// WriteChunk writes data to the chunk at specific offset.
	// data.size()+offset should be within chunk size.
	GFSError
	WriteChunk(ChunkHandle handle, std::uint64_t offset, const std::vector<char> &data)
	{
        std::string _data;
        for(unsigned int i = 0; i < data.size(); ++i) {
            _data += data[i];
        }
        
        std::uint64_t dataId = (time(nullptr) << 32) | (int)((rand() % RAND_MAX) * (rand() % RAND_MAX));
        std::tuple < GFSError, std::vector<std::string> > ret;
        call(ret, "master", "RPCGetReplicas", handle);//pushdata
	if (!std::get<0>(ret).is_ok()) return std::get<0>(ret);
	unsigned sz = std::get<1>(ret).size();
    assert(sz);
	dbg<<"rep sz:"<<sz<<std::endl;
	for(unsigned int i = 0; i < sz; ++i) {
//            LightDS::User::RPCAddress address;
//            address.ip = std::get<1>(ret)[i];
//            std::vector<LightDS::User::RPCAddress> _address = srv.ListService("chunkserver");
//            for(int j = 0; j < _address.size(); ++j) {
//                if(_address[j].ip == address.ip) {
//                    address.port = _address[j].port;
//                    break;
//                }
//            }
//	    dbg << "push:" << address.ip << std::endl;
	GFSError err;
	bool t = call(err, LightDS::User::RPCAddress::from_string(std::get<1>(ret)[i]), "RPCPushData", dataId, _data);
        if (!(t && err.is_ok()))
        {
            dbg << "pushdata failed at:" << std::get<1>(ret)[i] << std::endl;
        }
	//if (!(t && err.is_ok())) return err; 
        //try{
            //GFSError t = srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(ret)[i]), "RPCPushData", dataId, _data).get().as<GFSError>();
            //if(!t.is_ok()) return t;
        //}catch(...){
            //std::cerr<<"call chunkserver failed"<<std::endl;
        //}
        }
        
        std::tuple < GFSError, std::string, std::vector<std::string>, std::uint64_t > tmp;
        call(tmp, "master", "RPCGetPrimaryAndSecondaries", handle);
        if(!std::get<0>(tmp).is_ok()) return std::get<0>(tmp);
        
//        LightDS::User::RPCAddress address;
//        address.ip = std::get<1>(tmp);
//        std::vector<LightDS::User::RPCAddress> _address = srv.ListService("chunkserver");
//        for(int j = 0; j < _address.size(); ++j) {
//            if(_address[j].ip == address.ip) {
//                address.port = _address[j].port;
//                break;
//            }
//        }
        GFSError result;
        try{
            result = srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(tmp)), "RPCWriteChunk", handle, dataId, offset, std::get<2>(tmp)).get().as<GFSError>();
        }catch(...) {
            dbg<<"call server failed"<<std::endl;
        }
        return result;
	}

	// AppendChunk appends data to a chunk.
	// Chunk offset of the start of data will be returned if success.
	// data.size() should be within max append size.
	std::tuple < GFSError, std::uint64_t /*offset*/ >
    AppendChunk(ChunkHandle handle, const std::vector<char> &data) {
        std::string _data;
        for(unsigned int i = 0; i < data.size(); ++i) {
            _data += data[i];
        }
        std::uint64_t dataId = (time(nullptr) << 32) | (int)((rand() % RAND_MAX) * (rand() % RAND_MAX));
        std::tuple < GFSError, std::vector<std::string> > ret;
        call(ret, "master", "RPCGetReplicas", handle);//pushdata
        for(unsigned int i = 0; i < std::get<1>(ret).size(); ++i) {
//            LightDS::User::RPCAddress address;
//            address.ip = std::get<1>(ret)[i];
//            std::vector<LightDS::User::RPCAddress> _address = srv.ListService("chunkserver");
//            for(int j = 0; j < _address.size(); ++j) {
//                if(_address[j].ip == address.ip) {
//                    address.port = _address[j].port;
//                    break;
//                }
//            }
            //try{
                //GFSError t = srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(ret)[i]), "RPCPushData", dataId, _data).get().as<GFSError>();
                //if(!t.is_ok()) return std::tuple < GFSError, std::uint64_t > (t, 0);
            //}catch(...){
                //std::cerr<<"call server failed"<<std::endl;
            //}
	    GFSError err;
	    bool t = call(err, LightDS::User::RPCAddress::from_string(std::get<1>(ret)[i]), "RPCPushData", dataId, _data); 
        if (!(t && err.is_ok()))
        {
            dbg << "pushdata failed at:" << std::get<1>(ret)[i] << std::endl;
        }
	    //if (!(t && err.is_ok())) return std::tuple < GFSError, std::uint64_t > (err, 0); 
        }
        
        std::tuple < GFSError, std::string, std::vector<std::string>, std::uint64_t > tmp;
        call(tmp, "master", "RPCGetPrimaryAndSecondaries", handle);
        if(!std::get<0>(tmp).is_ok()) return std::tuple < GFSError, std::uint64_t >(std::get<0>(tmp), 0);
        
//        LightDS::User::RPCAddress address;
//        address.ip = std::get<1>(tmp);
//        std::vector<LightDS::User::RPCAddress> _address = srv.ListService("chunkserver");
//        for(int j = 0; j < _address.size(); ++j) {
//            if(_address[j].ip == address.ip) {
//                address.port = _address[j].port;
//                break;
//            }
//        }
        std::tuple < GFSError, std::uint64_t /*offset*/ > result;
        try{result = srv.RPCCall(LightDS::User::RPCAddress::from_string(std::get<1>(tmp)), "RPCAppendChunk", handle, dataId, std::get<2>(tmp)).get().as<std::tuple < GFSError, std::uint64_t >>();
        }catch(...){
            dbg<<"call server failed"<<std::endl;
        }
        return result;
    }

protected:
	LightDS::User &srv;
};

#endif
