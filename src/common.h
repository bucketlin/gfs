#ifndef GFS_COMMON_H
#define GFS_COMMON_H

#include <msgpack.hpp>

typedef std::uint64_t ChunkHandle;
typedef std::uint64_t ChunkVersion;

enum class GFSErrorCode : std::uint32_t
{
    OK = 0,
    m_no_such_file,
    m_dir_not_empty,
    m_file_exists,
    m_is_a_dir,
    m_is_a_file,
    m_invalid_index,//?
    m_invalid_filename,
    m_no_such_server,
    m_server_exists,
    m_server_not_found,
    m_not_enough_servers,
    m_rep_exists,
    m_chunk_exists,
    m_no_such_rep,
    m_no_such_chunk,
    m_oplog_not_exist,
    m_oplog_replay_error,
    s_creation_failed,
    s_invalid_data,
    s_no_such_file,
    s_exceed_set_size,
    s_exist_dataId,
    s_no_such_data,
    s_chunk_filled_up,
    s_write_failed,
    s_pad_failed,
    s_append_failed,
    s_copy_failed,
};

#define DEBUG 1
struct GFSError
{
	GFSErrorCode errCode;
	std::string description;
	GFSError(): errCode(GFSErrorCode::OK), description("") {}
	GFSError(GFSErrorCode e, std::string d): errCode(e), description(d) 
	{
		if(DEBUG) std::cerr<<'['<<(int)e<<"] desc:"<<d<<std::endl;
	}
	bool is_ok()
	{
		return errCode == GFSErrorCode::OK;
	}

	MSGPACK_DEFINE(errCode, description);
};

MSGPACK_ADD_ENUM(GFSErrorCode);

#define CUR "nil"
#define dbg if(DEBUG) std::cerr<<time(nullptr)<<" ("<<CUR<<") "<<__FUNCTION__<<'@'<<__LINE__<<": "
#define dbga if(DEBUG) std::cerr
//#define dbg if(DEBUG) std::cerr<<time(nullptr)<<' '<<CUR<<'['<<std::this_thread::get_id()<<"] "<<__FUNCTION__<<'@'<<__LINE__<<": "
#define gthrow(x,d) {if(DEBUG) std::cerr << "["<<#x<<"] DESC:"<<d<<" LINE:"<<__LINE__<<" FUNC:"<<__FUNCTION__<<std::endl; return GFSError(GFSErrorCode::x,d);}
//#define gthrow(x,d); return GFSError(GFSErrorCode::x,d);
#define EOK GFSError()
#define ERR(x,d) GFSError(GFSErrorCode::x,#x+d)
#define GOK return GFSError();
#endif
