#include <iostream>
#include <fstream>
#include "../master.h"
#define TEST 0
int main()
{
	freopen("./master_debug_log.log", "w", stderr);
	std::ofstream mlog("test_master_log.log");
	LightDS::Service msrv("master", mlog, std::cin, std::cout, 11235);
	Master mas(msrv, "./test_master");
#if TEST
	mas.test();
#else
	mas.Start();
	mas.Shutdown();
#endif
	return 0;
}

