#include <iostream>
#include "../client.h"
int main(int argc, char **argv)
{
	LightDS::User::RPCAddress node_srv;
	if(argc > 1) node_srv = LightDS::User::RPCAddress::from_string(argv[1]);
	LightDS::PipeService pipe(std::cin, std::cout);
//	LightDS::User usr(node_srv);
	LightDS::User usr(pipe);
	Client cli(usr);
	std::cerr << "begin client test" << std::endl;
	GFSError err;
	err = cli.Mkdir("/test");
	err = cli.Mkdir("/test/1");
	err = cli.Create("/test/1/test_file.dat");
	std::string str("hello");
	std::vector<char> vec(str.begin(), str.end());
	err = cli.Write("/test/1/test_file.dat", 0, vec);
	return 0;
}
