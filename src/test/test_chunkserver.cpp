#include <iostream>
#include <fstream>
#include "../chunkserver.h"
int main()
{
	std::ofstream slog("test_cs_log.log");
	LightDS::Service csrv("chunkserver", slog, std::cin, std::cout, 11235);
	ChunkServer csv(csrv, "./test_chunkserver");
	csv.Start();
	csv.Shutdown();
	return 0;
}

