#ifndef GFS_CHUNKSERVER_H
#define GFS_CHUNKSERVER_H

#include "common.h"
#include <string>
#include <service.hpp>
#include <iostream>
#include <fstream>
#include <map>
#include "global.h"
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include <stdio.h>
#include <cassert>
#include <thread>
#include <mutex>
#include <atomic>

#ifdef CUR
#undef CUR
#endif
#define CUR "csrv_"<<rootDir[14] 
class ChunkServer
{
    template <typename Ret, typename ...Args> void bind(std::string rpcName, Ret(ChunkServer::*func)(Args...))
    {
        srv.RPCBind(rpcName, std::function<Ret(Args...)>([this, func](Args ...args)->Ret {return (this->*func)(std::forward<Args>(args)...);}));
    }
    #define BIND(x) bind(#x, &ChunkServer::x);
public:
    ChunkServer(LightDS::Service &srv, const std::string &rootDir): srv(srv), rootDir(rootDir)
    {
        //BIND(Heartbeat)
        BIND(RPCCreateChunk)
        BIND(RPCReadChunk)
        BIND(RPCWriteChunk)
        BIND(RPCAppendChunk)
        BIND(RPCApplyMutation)
        BIND(RPCSendCopy)
        BIND(RPCApplyCopy)
        BIND(RPCGrantLease)
        BIND(RPCUpdateVersion)
        BIND(RPCPushData)
    }
	enum MutationType : std::uint32_t
	{
	    MutationWrite,
	    MutationAppend,
	    MutationPad
	};
    struct chunkInformation{
        bool isPrimary;
        ChunkVersion version;
        std::uint64_t timeStamp;
        std::uint64_t serialNo;
        std::mutex _lock;
        chunkInformation(){
            serialNo = 0;
            isPrimary = 0;
            timeStamp = 0;
            version = 1;
        }
        chunkInformation(bool x, ChunkVersion y, std::uint64_t z, std::uint64_t w){
            isPrimary = x;
            version = y;
            timeStamp = z;
            serialNo = w;
        }
        chunkInformation(chunkInformation const &tmp){
            isPrimary = tmp.isPrimary;
            version = tmp.version;
            timeStamp = tmp.timeStamp;
            serialNo = tmp.serialNo;
        }
        chunkInformation& operator=(const chunkInformation & tmp)//重载运算符
           {
               isPrimary = tmp.isPrimary;
               version = tmp.version;
               timeStamp = tmp.timeStamp;
               serialNo = tmp.serialNo;
               return * this;
            }
    };
private:
    int up;
    std::map<std::uint64_t, std::string> dataMap;
    std::map<ChunkHandle, chunkInformation> chunkMap;
    void touch_dir()
    {
        struct dirent *ent = NULL;
        std::string fileName;
        DIR *dir = opendir(rootDir.c_str());
        if (dir != NULL) {
            while ((ent = readdir (dir)) != NULL) {
                if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
                    fileName = ent->d_name;
//                    std::cout<<ent->d_name<<std::endl;
                    if(fileName[0] >= '0' && fileName[0] <= '9') {
                        std::string _handle;
                        for(unsigned int i = 0; i < fileName.size(); ++i) {
                            if(fileName[i] == 'v'){
                                const char * _fileName = _handle.c_str();
                                uint64_t handle =  strtoull (_fileName, NULL, 0);
                                std::ifstream fin(rootDir + fileName);
                                std::string _version;
                                fin>>_version;
                                uint64_t version = strtoull (_version.c_str(), NULL, 0);
                                chunkMap[handle].version = version;
                                dbg << "load H:"<<handle<<" str:"<<_version<<" Ver:" <<version<<std::endl;
                                break;
                            }
                            if(fileName[i] == 's'){
                                const char * _fileName = _handle.c_str();
                                uint64_t handle =  strtoull (_fileName, NULL, 0);
                                std::ifstream fin(rootDir + fileName);
                                std::string _serialNo;
                                fin>>_serialNo;
                                uint64_t serialNo = strtoull (_serialNo.c_str(), NULL, 0);
                                chunkMap[handle].serialNo = serialNo;
                                dbg << "load H:"<<handle<<" str:"<<_serialNo<<" No:" <<serialNo<<std::endl;
                                break;
                            }
                            _handle += fileName[i];
                        }
                    }
                }
            }
            closedir(dir);
        }
    }
public:
    //ChunkServer(LightDS::Service &srv, const std::string &rootDir): srv(srv), rootDir(rootDir) {}
	void Start()
	{
		dbg << "Chunkserver started" << std::endl;
       	up = 1;
        touch_dir(); 
		std::thread th_hb(&ChunkServer::Heartbeat, this);
		th_hb.detach();
	}
	void Shutdown()
	{
		std::lock_guard<std::mutex> lock(mtx);
		up = 0;
		cv.notify_all();
		dbg << "Chunkserver shutdown" << std::endl;
	}

public:
    void Heartbeat(){
	std::unique_lock<std::mutex> lock(mtx);
	for ( ; up; cv.wait_for(lock, HB_INTERVAL))
	{
	dbg << "beating" << std::endl;
        std::vector<ChunkHandle> leaseExtensions;
        std::vector<std::tuple<ChunkHandle, ChunkVersion>> chunks;
        std::vector<ChunkHandle> failedChunks;
        
        for(auto iter = chunkMap.begin(); iter != chunkMap.end(); ++iter){
		//assert(iter->second);
            ChunkHandle handle = iter -> first;
            std::string fileName = std::to_string(handle);
            std::ifstream readFile(rootDir + fileName);
            if(!readFile) failedChunks.push_back(handle);
            readFile.close();
            if(iter -> second.isPrimary == 1) {
                if(std::chrono::seconds(time(nullptr) - iter -> second.timeStamp) > LEASE_TIMEOUT) {
                    //leaseExtensions.push_back(handle);
		    iter -> second.isPrimary = 0;
                }
            }
            chunks.push_back(std::tuple<ChunkHandle, ChunkVersion>(handle, iter -> second.version));
        }
        std::vector<LightDS::Service::RPCAddress> ad;
        dbg << "connecting master"<<std::endl;
        int att=0;
        while(ad.empty() && att < RPC_REDIAL){
        try{
            ++att;
            ad = srv.ListService("master");
        }catch(...){
           //dbg<<"master down...retrying"<<std::endl;
           //std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        }
        if (ad.empty())
        {
            dbg << "connect failed, abort" << std::endl;
            continue;
        }
        dbg << "master connected:"<<ad[0].ip<<" attempt:"<<att<<std::endl;
//	if (ad.empty()) continue;
        LightDS::Service::RPCAddress address = ad[0];
	try{
        auto result = srv.RPCCall(address, "RPCHeartbeat", leaseExtensions, chunks, failedChunks).get().as< std::tuple< GFSError, std::vector<ChunkHandle> > >();
        if(std::get<0>(result).is_ok()) {
            std::vector<ChunkHandle> garbage = std::get<1>(result);
            for(unsigned int i = 0; i < garbage.size(); ++i) {
                std::string fileName = std::to_string(garbage[i]);
                std::string file1 = rootDir + fileName + "version";
                std::string file2 = rootDir + fileName + "serialNo";
		std::string path = rootDir + fileName;
                std::remove(path.c_str());
                std::remove(file1.c_str());
                std::remove(file2.c_str());
                chunkMap.erase(chunkMap.find(garbage[i]));
            }
        }
	}
	catch (...)
	{
		dbg << "HBgg" << std::endl;
	}
	//std::this_thread::sleep_for(HB_INTERVAL);
	}
    }

	// RPCCreateChunk is called by master to create a new chunk given the chunk handle.
	GFSError
    RPCCreateChunk(ChunkHandle handle){
        std::string fileName = std::to_string(handle);
        std::ofstream creatFile(rootDir + fileName);
        
        if(!creatFile){
            gthrow(s_creation_failed, "can't create");
        }
        else{
            creatFile.close();
            chunkMap[handle] = chunkInformation();
	    dbg << "H:" << handle <<std::endl;
            return GFSError();
        }
    }

	// RPCReadChunk is called by client, read chunk data and return
	std::tuple < GFSError, std::string /*Data*/ >
    RPCReadChunk(ChunkHandle handle, std::uint64_t offset, std::uint64_t length) {
        std::string fileName = std::to_string(handle);
        std::ifstream readFile(rootDir + fileName);
        
        std::string data;
        if(!readFile)
            return std::tuple < GFSError, std::string >(GFSError(GFSErrorCode::s_no_such_file, fileName), data);
        chunkMap[handle]._lock.lock();
        dbg<<handle<<" "<<offset<<" "<<offset + length<<std::endl;
        readFile.seekg(offset);
        assert(readFile.tellg()==offset);
        char *str = new char[CHUNK_SIZE + 10]{0};
        readFile.read(str, sizeof(char)*length);
        data.assign(str,length);
        delete[] str;
        //if (data.size() != length)
        if (readFile.eof())
        {
            dbg<<"unexpected eof: read:"<<data.size()<<'/'<<length<<std::endl;
		    return std::tuple < GFSError, std::string >(ERR(s_invalid_data, std::to_string(data.size())), data);
        }
        //for(std::uint64_t i = 0; i < length; ++i) {
            //char letter;
            //readFile.read(reinterpret_cast<char*>(&letter), sizeof(char));
		//if(readFile.eof()) {
////		readFile.close();
		////data.pop_back();
		//dbg<<"unexpected eof: read:"<<i<<'/'<<length<<std::endl;
		//return std::tuple < GFSError, std::string >(ERR(s_invalid_data, std::to_string(i)), data);
		////break;
		//}
            //data += letter;
        //}
	    dbg << "tellg:" <<  readFile.tellg() << std::endl;
        readFile.close();
        chunkMap[handle]._lock.unlock();
        return std::tuple < GFSError, std::string >(GFSError(), data);
    }

	// RPCWriteChunk is called by client
	// applies chunk write to itself (primary) and asks secondaries to do the same.
	GFSError
    RPCWriteChunk(ChunkHandle handle, std::uint64_t dataID, std::uint64_t offset, std::vector<std::string> secondaries) {
	dbg<<"H:"<<handle<<" dataID:"<<dataID<<" offset:"<<offset<<" sec:"<<secondaries.size()<<std::endl;
        
        std::string fileName = std::to_string(handle);
        
       //chunkMap[handle]._lock.lock();
        std::unique_lock<std::mutex> lock(chunkMap[handle]._lock);
        std::fstream writeChunk(rootDir + fileName, std::ios::in|std::ios::out|std::ios::binary);
        
        std::string file1 = fileName + "serialNo";
        
        if(!writeChunk)
            return GFSError(GFSErrorCode::s_no_such_file, fileName);
        
        
        writeChunk.seekg( 0, std::ios::end );
        dbg<<"end position"<<" "<<writeChunk.tellg()<<std::endl;
        {
            int len = offset - writeChunk.tellg();
            if(len > 0)
            {
                char *str = new char[len]{0};
                writeChunk.write(str, sizeof(char) * len);
                delete []str;
            }
        }
        //while(writeChunk.tellg() < offset) {
            //char letter = 0;
            //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
        //}
        
        writeChunk.seekp(offset);
        
        if(dataMap.find(dataID) == dataMap.end())
            return GFSError(GFSErrorCode::s_no_such_data, std::to_string(dataID));
        
        std::string data = dataMap[dataID];
        writeChunk.write(data.c_str(), sizeof(char)*data.size());
        //for(unsigned int i = 0; i < data.size(); ++i){
            //char letter = data[i];
            //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
        //}
        dbg<<handle<<" *"<<offset<<" "<<offset + data.size()<<std::endl;
        writeChunk.close();
        
        
        if(chunkMap.find(handle) == chunkMap.end())
            return GFSError(GFSErrorCode::s_no_such_file, fileName);
        
        std::uint64_t serialNo = ++chunkMap[handle].serialNo;
        
        std::ofstream fout(rootDir + file1);
        fout<<serialNo;
        fout.close();
        lock.unlock();
        bool error = 0;
        for(unsigned int i = 0; i < secondaries.size(); ++i) {
//            std::vector<LightDS::Service::RPCAddress> _address = srv.ListService("chunkserver");
//            LightDS::Service::RPCAddress address;
//            address.ip = secondaries[i];
//            for(int j = 0; j < _address.size(); ++j) {
//                if(_address[j].ip == secondaries[i]) {
//                    address.port = _address[j].port;
//                    break;
//                }
//            }
            try{if(!srv.RPCCall(LightDS::Service::RPCAddress::from_string(secondaries[i]), "RPCApplyMutation", handle, serialNo, MutationWrite, dataID, offset, data.size()).get().as<GFSError>().is_ok()){
                error = 1;
		break;
            }
            }catch(...){
               dbg<<"call secondaries failed"<<std::endl;
            }
	    dbg<<"sec"<<i<<" OK"<<std::endl;
        }
        //to send to secondaries
        if(error) gthrow(s_write_failed, "");
	dbg<<"H:"<<handle<<" dataID:"<<dataID<<" Written"<<std::endl;
        GOK;
    }

	// RPCAppendChunk is called by client to apply atomic record append.
	// The length of data should be within max append size.
	// If the chunk size after appending the data will excceed the limit,
	// pad current chunk and ask the client to retry on the next chunk.
	std::tuple < GFSError, std::uint64_t /*offset*/ >
    RPCAppendChunk(ChunkHandle handle, std::uint64_t dataID, std::vector<std::string> secondaries){
	    dbg<<"H:"<<handle<<" dataID:"<<dataID<<" sec:"<<secondaries.size()<<std::endl;
        std::string fileName = std::to_string(handle);
        //std::lock_guard<std::mutex> lock(chunkMap[handle]._lock);
        std::fstream writeChunk(rootDir + fileName, std::ios::in|std::ios::out|std::ios::binary);
        
        std::string file1 = fileName + "serialNo";
        
        if(!writeChunk)
            return std::tuple< GFSError, std::uint64_t > (GFSError(GFSErrorCode::s_no_such_file, "can't open file"), 0);
        
        if(dataMap.find(dataID) == dataMap.end())
            return std::tuple< GFSError, std::uint64_t > (GFSError(GFSErrorCode::s_no_such_data, "dataId not exist"), 0);
        chunkMap[handle]._lock.lock();
        std::string data = dataMap[dataID];
        writeChunk.seekg( 0, std::ios::end );
        int size = writeChunk.tellg();
        std::uint64_t serialNo = ++chunkMap[handle].serialNo;
        
        std::ofstream fout(rootDir + file1);
        fout<<serialNo;
        fout.close();
        
        writeChunk.seekp(0, std::ios::end);
        if(size + data.size() > CHUNK_SIZE){
            {
                int len = CHUNK_SIZE - writeChunk.tellg();
                if(len > 0)
                {
                    char *str = new char[len]{0};
                    writeChunk.write(str, sizeof(char) * len);
                    delete []str;
                }
            }
            //while(writeChunk.tellg() != CHUNK_SIZE){
                //char letter = 0;
                //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
            //}
            chunkMap[handle]._lock.unlock();
            
            bool error = 0;
            for(unsigned int i = 0; i < secondaries.size(); ++i) {
//                std::vector<LightDS::Service::RPCAddress> _address = srv.ListService("chunkserver");
//                LightDS::Service::RPCAddress address;
//                address.ip = secondaries[i];
//                for(int j = 0; j < _address.size(); ++j) {
//                    if(_address[j].ip == secondaries[i]) {
//                        address.port = _address[j].port;
//                        break;
//                    }
//                }
                try{
                if(!srv.RPCCall(LightDS::Service::RPCAddress::from_string(secondaries[i]), "RPCApplyMutation", handle, serialNo, MutationPad, dataID, 0, data.size()).get().as<GFSError>().is_ok()){
                    error = 1;
                }
                }catch(...){
                    dbg<<"call secondaries failed"<<std::endl;
                }
            }
            
            //pad secondaries
            if(error)
                return std::tuple< GFSError, std::uint64_t > (GFSError(GFSErrorCode::s_pad_failed,""), 0);
            else
                return std::tuple< GFSError, std::uint64_t > (GFSError(GFSErrorCode::s_chunk_filled_up, "chunk is full"), 0);
        }
        
        else{
            int pos = writeChunk.tellp();
            //dbg << "tellp:" << writeChunk.tellp();
            writeChunk.write(data.c_str(), sizeof(char)*data.size());
            //for(unsigned int i = 0; i < data.size(); ++i){
                //char letter = data[i];
                //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
            //}
            dbg << "befp:"<<pos<<" len:"<<data.size()<<" afterp:"<<writeChunk.tellp()<<std::endl;
            writeChunk.close();
            chunkMap[handle]._lock.unlock();
            
            bool error = 0;
            for(unsigned int i = 0; i < secondaries.size(); ++i) {
//                std::vector<LightDS::Service::RPCAddress> _address = srv.ListService("chunkserver");
//                LightDS::Service::RPCAddress address;
//                address.ip = secondaries[i];
//                for(int j = 0; j < _address.size(); ++j) {
//                    if(_address[j].ip == secondaries[i]) {
//                        address.port = _address[j].port;
//                        break;
//                    }
//                }
                try{
                if(!srv.RPCCall(LightDS::Service::RPCAddress::from_string(secondaries[i]), "RPCApplyMutation", handle, serialNo, MutationAppend, dataID, 0, data.size()).get().as<GFSError>().is_ok()){
                    error = 1;
                }
                }catch(...){
                    dbg<<"call secondaries failed"<<std::endl;
                }
            }
                
            //append secondaries
            if(error)
                return std::tuple< GFSError, std::uint64_t > (GFSError(GFSErrorCode::s_append_failed,""), size);
            else
                return std::tuple< GFSError, std::uint64_t > (GFSError(), size);
        }
    }

	// RPCApplyMutation is called by primary to apply mutations
	GFSError
    RPCApplyMutation(ChunkHandle handle, std::uint64_t serialNo, MutationType type, std::uint64_t dataID, std::uint64_t offset, std::uint64_t length){
	dbg<<"H:"<<handle<<" No:"<<serialNo<<" dataID:"<<dataID<<" offset:"<<offset<<" len:"<<length<<std::endl;
        std::string fileName = std::to_string(handle);
        std::fstream writeChunk(rootDir + fileName, std::ios::in|std::ios::out|std::ios::binary);
        
        std::string file1 = fileName + "serialNo";
        
        if(!writeChunk)
            return GFSError(GFSErrorCode::s_no_such_file, "can't open file");
        
        if(dataMap.find(dataID) == dataMap.end())
            return GFSError(GFSErrorCode::s_no_such_data, "dataId not exist");
        
        if(chunkMap.find(handle) == chunkMap.end())
            return GFSError(GFSErrorCode::s_no_such_file, "can't find chunk");
        
        //judge
        while(chunkMap[handle].serialNo + 1 != serialNo){}
        chunkMap[handle]._lock.lock();
        chunkMap[handle].serialNo++;
        
        
        std::ofstream fout(rootDir + file1);
        fout<<serialNo;
        fout.close();
        
        std::string data = dataMap[dataID];
        switch(type){
            case MutationWrite:
                writeChunk.seekg(0, std::ios::end);
                {
                int len = offset - writeChunk.tellg();
                if(len > 0)
                {
                    char *str = new char[len]{0};
                    writeChunk.write(str, sizeof(char) * len);
                    delete []str;
                }
                }
                //while(writeChunk.tellg() < offset) {
                    //char letter = 0;
                    //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
                //}
                writeChunk.seekp(offset);
                writeChunk.write(data.c_str(), sizeof(char) * data.size());
                //for(unsigned int i = 0; i < data.size(); ++i){
                    //char letter = data[i];
                    //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
                //}
                break;
                
            case MutationAppend:
                writeChunk.seekg( 0, std::ios::end );
                writeChunk.write(data.c_str(), sizeof(char) * data.size());
                //for(unsigned int i = 0; i < data.size(); ++i){
                    //char letter = data[i];
                    //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
                //}
                break;
                
            case MutationPad:
                writeChunk.seekg( 0, std::ios::end );
                {
                int len = CHUNK_SIZE - writeChunk.tellg();
                if(len > 0)
                {
                    char *str = new char[len]{0};
                    writeChunk.write(str, sizeof(char) * len);
                    delete []str;
                }
                }
                //while(writeChunk.tellg() != CHUNK_SIZE){
                    //char letter = 0;
                    //writeChunk.write(reinterpret_cast<char*>(&letter), sizeof(char));
                //}
                break;
        }
        writeChunk.close();
        chunkMap[handle]._lock.unlock();
	dbg<<"H:"<<handle<<" No:"<<serialNo<<" dataID:"<<dataID<<" Applied"<<std::endl;
        GOK;
    }

	// RPCSendCopy is called by master, send the whole copy to given address
	GFSError
    RPCSendCopy(ChunkHandle handle, std::string addr){
        std::string fileName = std::to_string(handle);
        std::ifstream readFile(rootDir + fileName);
        
        if(!readFile)
            return GFSError(GFSErrorCode::s_no_such_file, "open file failed");
        
        std::string data;
        readFile.seekg(0, std::ios::end);
        int len = readFile.tellg();
        if (len > 0)
        {
        char *str=new char[len + 5];
        readFile.seekg(0, std::ios::beg);
        readFile.read(str, sizeof(char) * len);
        data.assign(str, len);
        delete []str;
        }
        //while(1){
            //char letter;
            //readFile.get(letter);
            ////readFile.read(reinterpret_cast<char*>(&letter), sizeof(char));
            //if (readFile.eof()) break;
            //data += letter;
        //}
        readFile.close();
        
        if(chunkMap.find(handle) == chunkMap.end())
            return GFSError(GFSErrorCode::s_no_such_file, "can't find chunk");
        
        ChunkVersion version = chunkMap[handle].version;
        std::uint64_t serialNo = chunkMap[handle].serialNo;
        
//        std::vector<LightDS::Service::RPCAddress> _address = srv.ListService("chunkserver");
//        LightDS::Service::RPCAddress address;
//        address.ip = addr;
//        for(int j = 0; j < _address.size(); ++j) {
//            if(_address[j].ip == addr) {
//                address.port = _address[j].port;
//                break;
//            }
//        }
        try{
        if(srv.RPCCall(LightDS::Service::RPCAddress::from_string(addr), "RPCApplyCopy", handle, version, data, serialNo).get().as<GFSError>().is_ok())
        //send to address
            GOK
        else
            return GFSError(GFSErrorCode::s_copy_failed, "");
        }catch(...){
            dbg<<"call secondaries failed"<<std::endl;
        }
        return GFSError(GFSErrorCode::s_copy_failed, "");
    }

	// RPCApplyCopy is called by another replica
	// rewrite the local version to given copy data
	GFSError
    RPCApplyCopy(ChunkHandle handle, ChunkVersion version, std::string data, std::uint64_t serialNo){
        std::string fileName = std::to_string(handle);
        std::lock_guard<std::mutex> lock(chunkMap[handle]._lock);
        std::ofstream copyFile(rootDir + fileName);
        
        std::string file1 = fileName + "serialNo";
        std::string file2 = fileName + "version";
        
        if(!copyFile)
            return GFSError(GFSErrorCode::s_creation_failed, "can't copy");
        
        copyFile.write(data.c_str(), sizeof(char)*data.size());
        //for(unsigned int i = 0; i < data.size(); ++i){
            //char letter = data[i];
            //copyFile.write(reinterpret_cast<char*>(&letter), sizeof(char));
        //}
        copyFile.close();
        
        std::ofstream fout1(rootDir + file1);
        fout1<<serialNo;
        fout1.close();
        
        std::ofstream fout2(rootDir + file2);
        fout2<<version;
        fout2.close();
        
        chunkMap[handle] = chunkInformation(0, version, 0, serialNo);
	dbg << "H:"<<handle<< " V:"<<version << " len:"<< data.length()<< " Copied" <<std::endl;
        GOK;
    }

	// RPCGrantLease is called by master
	// mark the chunkserver as primary
	GFSError
    RPCGrantLease(std::vector < std::tuple < ChunkHandle /*handle*/, ChunkVersion /*newVersion*/, std::uint64_t /*expire timestamp*/ >> leaseMap){
        
        for(unsigned int i = 0; i < leaseMap.size(); ++i) {
            std::string fileName = std::to_string(std::get<0>(leaseMap[i]));
            std::string file2 = fileName + "version";
            
            if(chunkMap.find(std::get<0>(leaseMap[i])) == chunkMap.end())
                return GFSError(GFSErrorCode::s_no_such_file, "can't find chunk");
            
            std::ofstream fout2(rootDir + file2);
            fout2<<std::get<1>(leaseMap[i]);
            fout2.close();
            
            std::uint64_t serialNo = chunkMap[std::get<0>(leaseMap[i])].serialNo;
            chunkMap[std::get<0>(leaseMap[i])] = chunkInformation(1, std::get<1>(leaseMap[i]), std::get<2>(leaseMap[i]), serialNo);
        }
        GOK;
    }

	// RPCUpdateVersion is called by master
	// update the given chunks' version to 'newVersion'
	GFSError
    RPCUpdateVersion(ChunkHandle handle, ChunkVersion newVersion){
        std::string fileName = std::to_string(handle);
        std::string file2 = fileName + "version";
        
        if(chunkMap.find(handle) == chunkMap.end())
            return GFSError(GFSErrorCode::s_no_such_file, "can't find chunk");
        
        std::ofstream fout2(rootDir + file2);
        fout2<<newVersion;
        fout2.close();
        
        chunkMap[handle].version = newVersion;
        GOK;
    }

	// RPCPushDataAndForward is called by client.
	// It saves client pushed data to memory buffer.
	// This should be replaced by a chain forwarding.
	GFSError
    RPCPushData(std::uint64_t dataID, std::string data){
        
        if(dataMap.find(dataID) != dataMap.end())
            return GFSError(GFSErrorCode::s_exist_dataId, "dataId already exist");
        dataMap[dataID] = data;
	dbg<<"ID:"<<dataID<<" len:"<<data.length()<<std::endl;	
        GOK;
    }

protected:
	LightDS::Service &srv;
	std::string rootDir;
	std::mutex mtx;
	std::condition_variable cv;
};

MSGPACK_ADD_ENUM(ChunkServer::MutationType);

#endif
