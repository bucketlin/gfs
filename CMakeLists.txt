cmake_minimum_required (VERSION 2.6)
project (GFS)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -w")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g3")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2")

set (EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

include_directories ("~/boost_1_64_0")
include_directories ("${PROJECT_SOURCE_DIR}/jsoncpp/include")
include_directories ("${PROJECT_SOURCE_DIR}/msgpack-c/include")
include_directories ("${PROJECT_SOURCE_DIR}/lightds/include")
include_directories ("${PROJECT_SOURCE_DIR}/src")
link_directories (~/boost_1_64_0/stage/lib)
link_directories (${PROJECT_SOURCE_DIR}/lib)

#add_subdirectory (jsoncpp)
#add_subdirectory (msgpack-c)

file(GLOB gray_src ${PROJECT_SOURCE_DIR}/test/*.cpp)
add_executable(graybox ${gray_src})
#add_executable(test_chunkserver ${PROJECT_SOURCE_DIR}/src/test/test_chunkserver.cpp)

#find_library(LIBJSONCPP libjsoncpp.a ${PROJECT_SOURCE_DIR}/jsoncpp/src/lib_json)
#set (EXTRA_LIBS ${EXTRA_LIBS} ${LIBJSONCPP})
set (EXTRA_LIBS ${EXTRA_LIBS} jsoncpp)
set (EXTRA_LIBS ${EXTRA_LIBS} msgpackc)
#set (EXTRA_LIBS ${EXTRA_LIBS} boost_regex)
set (EXTRA_LIBS ${EXTRA_LIBS} boost_system)
#set (EXTRA_LIBS ${EXTRA_LIBS} boost_date_time)
set (EXTRA_LIBS ${EXTRA_LIBS} boost_thread)
set (EXTRA_LIBS ${EXTRA_LIBS} boost_filesystem)
#set (EXTRA_LIBS ${EXTRA_LIBS} wsock32)
#set (EXTRA_LIBS ${EXTRA_LIBS} ws2_32)

target_link_libraries (graybox  ${EXTRA_LIBS})
#target_link_libraries (test_chunkserver  ${EXTRA_LIBS})

#install (TARGETS test_master DESTINATION bin/test)
#install (TARGETS test_chunkserver DESTINATION bin/test)
